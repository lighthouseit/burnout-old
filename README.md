# Burnout

> Your apps, now HOT!

[Português](https://gitlab.com/lighthouseit/burnout/blob/master/README.md) | [English](https://gitlab.com/lighthouseit/burnout/blob/master/README.en.md)

![Lighthouse Utils](https://i.imgur.com/VpKKfIM.png)

O boilerplate para React Native oficial da Lighthouse

Esse é o boilerplate que a equipe da Lighthouse recomenda e utiliza no dia-a-dia.

Recomendamos também que utilize nossa CLI [Lighthouse Tools](https://gitlab.com/lighthouseit/lighthouse-tools) para iniciar seu desenvolvimento de maneira rápida.

# Guias

- [Guia de configuração](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP.md) - _Obrigatório_
- [Configurando CI/CD](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-CI.md)
- [Instalando iOS CocoaPods](https://gitlab.com/lighthouseit/burnout/blob/master/docs/INSTALL-PODS.md)
- [Configurando Facebook SDK](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-FACEBOOK-SDK.md) - _Obrigatório_
- [Configurando Google Signin](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-GOOGLE-SIGNIN.md)

# Início rápido

Utilizando o `Lighthouse Tools`:

```
$ yarn global add lighthouse-tools
$ lighthouse-tools init awesomeapp

// Instalando dependencias
$ cd awesomeapp
$ yarn install
$ cd ios && pod install

// Rodando no Android
$ yarn run-android

// Rodando no iOs
$ yarn run-ios

🚀  1, 2, 3... Ready!
```

Clonando o repositório:

```
$ git clone https://gitlab.com/lighthouseit/burnout awesomeapp

// Instalando dependencias
$ cd awesomeapp
$ yarn install
$ cd ios && pod install

// Rodando no Android
$ yarn run-android

// Rodando no iOs
$ yarn run-ios

🚀  1, 2, 3... Ready!
```

## Variáveis de ambiente

Nós encorajamos você a usar variáveis de ambiente em seu projeto, por causa disso nós usamos a biblioteca `react-native-config` para gerenciar nossas variáveis dentro dos arquivos JS.

Para começar, duplique e renomeie o arquivo `.env.example` para`.env` e leia as instruções dentro do arquivo.

## Módulos nativos inclusos

- lottie-react-native
- react-native-config
- react-native-device-info
- react-native-fast-image
- react-native-fbsdk
- react-native-google-signin
- react-native-google-analytics-bridge
- react-native-image-gallery
- react-native-languages
- react-native-linear-gradient
- react-native-maps
- react-native-masked-text
- react-native-permissions
- react-native-snap-carousel
- react-native-svg
- react-native-vector-icons

[Node.js 7.6+](https://nodejs.org) é obrigatório.

## Patrocinadores

[Burnout](https://gitlab.com/lighthouseit/burnout) é patrocinado pela [Lighthouse](https://lighthouseit.com.br/), um time de designers e desenvolvedores localizados em Porto Alegre/RS - Brasil. Nossas skills variam entre UI/UX, React, React Native, .NET Core e mais!
