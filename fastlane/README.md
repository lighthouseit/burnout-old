fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
### create_app_icons
```
fastlane create_app_icons
```
Create appicons for both Android and iOS
### update_platforms_info
```
fastlane update_platforms_info
```
Update the app identifier, display name and icon for alpha, beta and production releases

----

## iOS
### ios create_app_on_apple_developer
```
fastlane ios create_app_on_apple_developer
```
Create app on Apple Developer portal.
### ios signin
```
fastlane ios signin
```
Fetch or create certificates and provisioning profiles
### ios build
```
fastlane ios build
```
Build the iOS application.
### ios distribute
```
fastlane ios distribute
```
Distribute IPA to Appcenter.
### ios build_and_distribute
```
fastlane ios build_and_distribute
```
Bump versions and deploy to Appcenter.

----

## Android
### android signin
```
fastlane android signin
```
Fetch Android keystore from remote
### android build
```
fastlane android build
```
Build the Android application.
### android distribute
```
fastlane android distribute
```
Distribute APK to Appcenter
### android build_and_distribute
```
fastlane android build_and_distribute
```
Bump versions and deploy APK to Appcenter.

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
