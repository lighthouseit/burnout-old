import Colors from './Colors';
import Layout from './Layout';

export default {
  ...Layout,
  ...Colors,
};
