import Layout from './Layout';

const scale = Layout.scale();

export default {
  Paragraph: {
    fontFamily: 'Roboto-Bold',
    fontSize: scale * 32,
    color: 'white',
    marginVertical: scale * 24,
  },
};
