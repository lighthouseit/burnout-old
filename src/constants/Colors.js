const primaryColor = '#3e278e';
const secondaryColor = '#c71223';
const paperColor = '#fff';
const grayColor = '#f8f8f8';

export default {
  primaryColor,
  secondaryColor,
  paperColor,
  grayColor,
  statusBarColor: '#2d1c66',
  backgroundColor: '#4D52A3',
};
