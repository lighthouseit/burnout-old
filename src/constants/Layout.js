import { Dimensions } from 'react-native';
import { getStatusBarHeight } from '@utils/Device';

const { width, height } = Dimensions.get('window');

const getScale = (smallScreenScaling = 0.9, limitScale = true) => {
  let scale = height / 640;
  if (scale > 1 && limitScale) {
    scale = 1;
  } else if (scale < 1) {
    scale *= smallScreenScaling;
  }
  return scale;
};

export default {
  window: {
    width,
    height,
  },
  statusBarHeight: getStatusBarHeight(),
  isSmallDevice: width < 375,
  xsGutter: 4,
  smGutter: 8,
  mdGutter: 16,
  lgGutter: 24,
  xlGutter: 32,
  headerHeight: 48,
  scale: getScale,
};
