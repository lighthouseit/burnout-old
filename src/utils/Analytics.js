import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import Config from 'react-native-config';

const tracker = new GoogleAnalyticsTracker(Config.RN_GA_TRACKER);

export const hitScreen = (screenName) => {
  tracker.trackScreenView(screenName);
};

export const hitEvent = (category, action, label, value) => {
  tracker.trackEvent(category, action, label, value);
};
