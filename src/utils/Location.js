import { Platform } from 'react-native';
import Permissions from 'react-native-permissions';
import { isSimulator } from './Device';

export const getLocationAsync = () => {
  return new Promise(async (resolve, reject) => {
    await Permissions.request('location');
    if (Platform.OS === 'android' && isSimulator) {
      console.warn(`
        Get user Location can not work on Android emulator.
        Try it on a real device or Genymotion!
      `);
    }

    navigator.geolocation.getCurrentPosition(resolve, reject, {
      enableHighAccuracy: false,
      timeout: 20000,
      maximumAge: 1000,
    });
  });
};
