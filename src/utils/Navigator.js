import { createStackNavigator } from 'react-navigation';

export function defaultStackNavigator(routes, options) {
  return createStackNavigator(routes, {
    mode: 'card',
    headerMode: 'none',
    ...options,
  });
}
