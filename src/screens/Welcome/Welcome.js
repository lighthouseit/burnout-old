import React from 'react';
import { Text } from 'react-native';
import { Screen, SmartImage, View } from '@components';
import { Images, Typography } from '@constants';

const Welcome = () => {
  return (
    <Screen>
      <View hAlign="center" flexible>
        <Text style={Typography.Paragraph}>Burnout your apps!</Text>
        <SmartImage style={{ width: 260, height: 260 }} source={Images.burnout} />
      </View>
    </Screen>
  );
};

export default Welcome;
