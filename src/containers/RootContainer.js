import React from 'react';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import AppNavigator from '@navigation/AppNavigator';

class RootContainer extends React.PureComponent {
  componentDidMount() {
    this.requestDeviceToken();
  }

  requestNotificationPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
    } catch (error) {
      console.log(error);
    }
  };

  requestDeviceToken = async () => {
    await this.requestNotificationPermission();
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      const fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log(fcmToken);
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <AppNavigator />
      </React.Fragment>
    );
  }
}

export default connect()(RootContainer);
