import I18n from './i18n';

export * from './translate';
export default I18n;
