import emptyStates from './empty-states';
import errors from './errors';

module.exports = {
  emptyStates,
  errors,
};
