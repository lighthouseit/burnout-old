import RNLanguages from 'react-native-languages';
import I18n from 'i18n-js';

import pt from './locales/pt_BR';

I18n.locale = RNLanguages.language;
I18n.fallbacks = true;
I18n.defaultLocale = 'pt';
I18n.translations = { pt };

export default I18n;
