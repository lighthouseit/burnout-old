import I18n from './i18n';

/**
 * toCurrency
 * @param number - the number to be formatted
 */
export function toCurrency(number) {
  return I18n.toCurrency(number || 0, I18n.t('CURRENCY_FORMAT'));
}

/**
 * Translates text.
 *
 * @param key - I18n path key
 */
export function translate(key) {
  return key ? I18n.t(key) : null;
}

/**
 * Translates text with options.
 *
 * @param key - I18n path key
 */
export function translateWithOpts(key, opts) {
  return key ? I18n.t(key, opts) : null;
}
