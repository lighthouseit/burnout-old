import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { TabBarBottom, TabBarIcon } from '@components';
import Welcome from '@screens/Welcome';

import { defaultStackNavigator as DefaultStackNavigator } from './Helpers';

const WelcomeStack = DefaultStackNavigator({
  Welcome,
});

WelcomeStack.navigationOptions = {
  title: 'Welcome',
  tabBarLabel: 'Welcome',
  tabBarTestID: 'WelcomeTab',
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="home" />,
};

const FavoritesStack = DefaultStackNavigator({
  Favorites: Welcome,
});

FavoritesStack.navigationOptions = {
  title: 'Favorites',
  tabBarLabel: 'Favorites',
  tabBarTestID: 'FavoritesTab',
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="hearto" />,
};

const ProfileStack = DefaultStackNavigator({
  Favorites: Welcome,
});

ProfileStack.navigationOptions = {
  title: 'Profile',
  tabBarLabel: 'Profile',
  tabBarTestID: 'ProfileTab',
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="user" />,
};

const SettingsStack = DefaultStackNavigator({
  Favorites: Welcome,
});

SettingsStack.navigationOptions = {
  title: 'Settings',
  tabBarLabel: 'Settings',
  tabBarTestID: 'SettingsTab',
  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="setting" />,
};

const TabNavigator = createBottomTabNavigator(
  {
    WelcomeStack,
    FavoritesStack,
    ProfileStack,
    SettingsStack,
  },
  {
    tabBarOptions: {
      showLabel: false,
      style: {
        height: 48,
        borderTopWidth: 0,
        overflow: 'visible',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 14,
      },
    },
    animationEnabled: false,
    ...(Platform.OS === 'android' ? { tabBarComponent: TabBarBottom } : {}),
    tabBarPosition: 'bottom',
  },
);

export default createStackNavigator(
  {
    Tabs: TabNavigator,
  },
  {
    initialRouteName: 'Tabs',
    mode: 'card',
    headerMode: 'none',
  },
);
