import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import { createStackNavigator, NavigationActions } from 'react-navigation';
import { hitScreen } from '@utils/Analytics';
import { defaultStackNavigator as DefaultStackNavigator } from '@utils/Navigator';
import { Colors } from '@constants';
import { Header } from '@components';

// Screens
import Welcome from '@screens/Welcome';

const MainStack = DefaultStackNavigator(
  {
    Welcome: {
      screen: Welcome,
      navigationOptions: {
        header: <Header title="Powered by Lighthouse" />,
      },
    },
  },
  {
    headerMode: 'screen',
  },
);

const getActiveRouteName = (navigationState) => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
};

const AppNavigator = createStackNavigator(
  {
    Main: MainStack,
  },
  {
    initialRouteName: 'Main',
    mode: 'modal',
    headerMode: 'none',
  },
);

export default class extends Component {
  navigate = (routeName, params) => {
    this.navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      }),
    );
  };

  render() {
    const { props } = this;
    return (
      <AppNavigator
        ref={n => (this.navigator = n)}
        onNavigationStateChange={(prevState, currentState) => {
          const currentScreen = getActiveRouteName(currentState);
          const prevScreen = getActiveRouteName(prevState);

          StatusBar.setBarStyle('light-content');
          if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(Colors.statusBarColor);
          }

          if (prevScreen !== currentScreen) {
            // the line below uses the Google Analytics tracker
            hitScreen(currentScreen);
          }
        }}
        {...props}
      />
    );
  }
}
