import { combineReducers } from 'redux';
import { persistStore } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import configureStore from './configureStore';

// Reducers
import globalReducer from './globalReducer';

const rootPersistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['global'],
};

const reducers = combineReducers({
  global: globalReducer,
});

export default () => {
  const store = configureStore(rootPersistConfig, reducers);
  const persistor = persistStore(store);
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers; // eslint-disable-line
      store.replaceReducer(nextRootReducer);
    });
  }
  return { store, persistor };
};
