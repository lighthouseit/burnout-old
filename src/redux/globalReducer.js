import { handleActions } from 'redux-actions';
import * as globalActions from './actions/globalActions';

// State
const initialState = {
  loading: false,
  refreshing: false,
};

// Reducers
const reducer = handleActions(
  {
    [globalActions.showGlobalLoading]: state => ({
      ...state,
      loading: true,
    }),
    [globalActions.hideGlobalLoading]: state => ({
      ...state,
      loading: false,
    }),
  },
  initialState,
);

export default reducer;
