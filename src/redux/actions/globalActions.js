import { createAction } from 'redux-actions';

// Actions
export const showGlobalLoading = createAction('SHOW_GLOBAL_LOADING');
export const hideGlobalLoading = createAction('HIDE_GLOBAL_LOADING');
