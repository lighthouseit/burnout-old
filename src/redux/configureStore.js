import { createStore, applyMiddleware, compose } from 'redux';
import { persistReducer } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools'; // eslint-disable-line

export default (rootPersistConfig, reducers) => {
  const middleware = [thunkMiddleware];
  const enhancers = [];
  enhancers.push(applyMiddleware(...middleware));
  // eslint-disable-next-line
  const composeEnhancers = __DEV__ ? composeWithDevTools : compose;
  const rootReducer = persistReducer(rootPersistConfig, reducers);
  const store = createStore(rootReducer, composeEnhancers(...enhancers));
  return store;
};
