import React, { PureComponent } from 'react';
import FastImage from 'react-native-fast-image';
import Images from '@constants/Images';

class SmartImage extends PureComponent {
  state = {
    fallbackImage: null,
  };

  render() {
    const { source, ...props } = this.props;
    const { fallbackImage } = this.state;
    return (
      <FastImage
        {...props}
        source={fallbackImage || source}
        onError={() => this.setState({ fallbackImage: Images.fallback })}
      />
    );
  }
}

export default SmartImage;
