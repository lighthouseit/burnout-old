/* eslint-disable react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import _ from 'lodash';
import styled from 'styled-components';

const gutterPropTypes = PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']);
const flexAlignPropTypes = PropTypes.oneOf(['flex-start', 'center', 'flex-end']);

const StyledView = styled(View)(props => _.omitBy(
  {
    position: 'relative',
    flex: props.flexible ? 1 : null,
    flexDirection: props.row ? 'row' : 'column',
    marginHorizontal: props.marginHorizontal ? props.theme[`${props.marginHorizontal}Gutter`] : 0,
    marginVertical: props.marginVertical ? props.theme[`${props.marginVertical}Gutter`] : 0,
    paddingHorizontal: props.paddingHorizontal
      ? props.theme[`${props.paddingHorizontal}Gutter`]
      : 0,
    paddingVertical: props.paddingVertical ? props.theme[`${props.paddingVertical}Gutter`] : 0,
    alignItems: props.row ? props.vAlign : props.hAlign,
    alignSelf: 'stretch',
    justifyContent: props.row ? props.hAlign : props.vAlign,
    backgroundColor: props.bgColor,
    ...props.style,
  },
  _.isNull,
));

class Row extends Component {
  static propTypes = {
    marginHorizontal: gutterPropTypes,
    marginVertical: gutterPropTypes,
    paddingHorizontal: gutterPropTypes,
    paddingVertical: gutterPropTypes,
    vAlign: flexAlignPropTypes,
    hAlign: flexAlignPropTypes,
    flexible: PropTypes.bool,
    row: PropTypes.bool,
    bgColor: PropTypes.string,
  };

  static defaultProps = {
    marginHorizontal: null,
    marginVertical: null,
    paddingHorizontal: null,
    paddingVertical: null,
    vAlign: 'flex-start',
    hAlign: 'flex-start',
    flexible: false,
    row: false,
    bgColor: 'transparent',
  };

  render() {
    const { children, ...props } = this.props;
    return <StyledView {...props}>{children}</StyledView>;
  }
}

export default Row;
