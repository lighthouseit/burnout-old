import React, { Component } from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import _ from 'lodash';

import HeaderButton from './HeaderButton';
import SearchBar from '../SearchBar';
import HeaderTitle from './HeaderTitle';

const components = {
  title: (value, props) => ({
    centerComponent: (
      <HeaderTitle numberOfLines={1} {...props}>
        {value || ''}
      </HeaderTitle>
    ),
  }),
  hasHistory: (value, props) => ({
    leftComponent: value ? (
      <HeaderButton
        {...props}
        onPress={() => (props.onPress ? props.onPress() : props.navigation.goBack(null))}
        icon="arrow-left"
        testID="HeaderHistory"
      />
    ) : null,
  }),
  isModal: (value, props) => !props.hasHistory
    && props.isModal && {
    leftComponent: (
      <HeaderButton
        {...props}
        onPress={() => (props.onPress ? props.onPress() : props.navigation.goBack())}
        icon="close"
      />
    ),
  },
  hasSearch: (value, props) => ({
    centerComponent: props.onlyNavigate ? (
      <TouchableWithoutFeedback
        style={{ alignSelf: 'stretch' }}
        onPress={() => props.navigation.navigate('Search')}
        testID="HeaderSearchBarNavigate"
      >
        <View style={{ alignSelf: 'stretch' }} pointerEvents="box-only">
          <SearchBar />
        </View>
      </TouchableWithoutFeedback>
    ) : (
      <SearchBar
        {...props}
        testID="HeaderSearchBar"
        onChangeText={props.onSearch}
        onSubmitEditing={props.onSearchSubmit}
        value={props.query}
        displayClear
      />
    ),
  }),
};

/**
 * If source (usually state set by component) has undefined
 * property values, ignore those properties.
 * @param objValue
 * @param srcValue
 * @returns {*}
 */
function skipUndefined(objValue, srcValue) {
  return _.isUndefined(srcValue) ? objValue : srcValue;
}

const composeComponents = HeaderComponent => class extends Component {
  render() {
    const newProps = {};
    if (this.props.hasSearch && this.props.title) {
      console.warn("Can't use hasSearch and title props at the same time at Header");
    }

    _.forEach(this.props, (value, key) => {
      if (_.isFunction(components[key])) {
        _.assign(newProps, components[key](value, this.props));
      }
    });

    return <HeaderComponent {..._.assignWith(newProps, this.props, skipUndefined)} />;
  }
};

export default composeComponents;
