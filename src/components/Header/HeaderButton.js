import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styled from 'styled-components';
import { Icon } from '@components';

const Touchable = styled(TouchableOpacity)(props => ({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  paddingHorizontal: props.spacing || props.theme.smGutter,
  height: props.theme.headerHeight,
}));

const Accessory = styled(View)(props => ({
  marginLeft: props.theme.smGutter,
}));

export default ({ onPress, icon, color, renderAccessory, ...props }) => (
  <Touchable onPress={onPress} {...props}>
    <Icon name={icon} size={24} color={color || 'white'} />
    {renderAccessory && <Accessory>{renderAccessory}</Accessory>}
  </Touchable>
);
