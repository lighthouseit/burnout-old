import React from 'react';
import { Text } from 'react-native';
import styled from 'styled-components';

const StyledText = styled(Text)(props => ({
  fontSize: 16,
  fontWeight: 'bold',
  color: props.color || props.theme.paperColor,
  maxWidth: props.theme.window.width * 0.8,
}));

export default ({ children, ...props }) => <StyledText {...props}>{children}</StyledText>;
