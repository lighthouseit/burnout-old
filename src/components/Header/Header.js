import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StatusBar, View } from 'react-native';
import { SafeAreaView, withNavigation } from 'react-navigation';
import _ from 'lodash';
import styled from 'styled-components';

import composeComponents from './ComposeComponents';

const StyledSafeAreaView = styled(SafeAreaView)(props => ({
  backgroundColor: props.clear ? 'transparent' : props.theme.primaryColor,
  zIndex: 80,
  ...props.style,
}));

const StyledHeader = styled(View)(() => ({
  flexDirection: 'row',
  flexWrap: 'nowrap',
  justifyContent: 'space-between',
  height: 48,
  alignItems: 'center',
  backgroundColor: 'transparent',
}));

const StyledCenterComponent = styled(View)(props => _.omitBy(
  {
    flex: props.title || props.hasSearch ? 1 : null,
    alignItems: 'center',
    justifyContent: 'center',
  },
  _.isNil,
));

const StyledLeftComponent = styled(View)(props => _.omitBy(
  {
    flex: props.hasSearch ? null : 1,
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
  },
  _.isNil,
));

const StyledRightComponent = styled(View)(props => _.omitBy(
  {
    flex: props.hasSearch ? null : 1,
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },
  _.isNil,
));

const StyledBottomComponent = styled(View)({});

class Header extends Component {
  static propTypes = {
    leftComponent: PropTypes.element,
    centerComponent: PropTypes.element,
    rightComponent: PropTypes.element,
    bottomComponent: PropTypes.element,
    transparent: PropTypes.bool,
  };

  static defaultProps = {
    leftComponent: null,
    centerComponent: null,
    rightComponent: null,
    bottomComponent: null,
    transparent: false,
  };

  componentWillMount() {
    StatusBar.setBarStyle('light-content');
  }

  shouldComponentUpdate(nextProps) {
    return !_.isEqual(nextProps, this.props);
  }

  render() {
    const {
      title,
      leftComponent,
      rightComponent,
      centerComponent,
      bottomComponent,
      ...props
    } = this.props;

    return (
      <StyledSafeAreaView {...props} forceInset={{ top: 'always' }}>
        <StyledHeader>
          {(leftComponent || title) && (
            <StyledLeftComponent {...props}>{leftComponent}</StyledLeftComponent>
          )}
          <StyledCenterComponent {...props}>{centerComponent}</StyledCenterComponent>
          {(rightComponent || title) && (
            <StyledRightComponent {...props}>{rightComponent}</StyledRightComponent>
          )}
        </StyledHeader>
        {bottomComponent && <StyledBottomComponent>{bottomComponent}</StyledBottomComponent>}
      </StyledSafeAreaView>
    );
  }
}

const ComposedHeader = withNavigation(composeComponents(Header));

export default ComposedHeader;
