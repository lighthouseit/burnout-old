import React, { PureComponent } from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class Icon extends PureComponent {
  render() {
    const { iconSet = null, ...props } = this.props;
    if (iconSet === 'MaterialIcons') {
      return <MaterialIcons {...props} />;
    }
    return <AntDesign {...props} />;
  }
}

export default Icon;
