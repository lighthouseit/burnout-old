import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, TextInputProps, TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import { translate } from '@i18n';
import { Icon } from '@components';
import theme from '@constants/Theme';

const BAR_HEIGHT = 36;
const StyledSearchBar = styled(View)(props => ({
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'stretch',
  height: BAR_HEIGHT,
  borderRadius: BAR_HEIGHT / 2,
  marginHorizontal: props.theme.mdGutter,
  backgroundColor: props.theme.paperColor,
}));

const StyledIcon = styled(props => <Icon {...props} />)(props => ({
  color: '#ccc',
  paddingHorizontal: props.theme.smGutter,
}));

class SearchBar extends Component {
  static propTypes = {
    ...TextInputProps,
    displayClear: PropTypes.bool,
  };

  static defaultProps = {
    displayClear: false,
  };

  render() {
    return (
      <StyledSearchBar>
        <StyledIcon name="magnify" size={24} />
        <TextInput
          style={{
            flex: 1,
            paddingRight: theme.smGutter,
            height: BAR_HEIGHT,
          }}
          ref={(ref) => {
            this.textInput = ref;
          }}
          placeholder={translate('searchBarPlaceholder')}
          underlineColorAndroid="transparent"
          {...this.props}
        />
        {this.props.displayClear && (
          <TouchableOpacity
            onPress={() => this.textInput && this.textInput.clear()}
            hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
          >
            <StyledIcon name="close" size={20} />
          </TouchableOpacity>
        )}
      </StyledSearchBar>
    );
  }
}

export default SearchBar;
