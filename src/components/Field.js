import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  TouchableOpacity,
  View, // eslint-disable-line
  findNodeHandle,
  TextInput,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import TextInputState from 'react-native/lib/TextInputState';
import styled from 'styled-components';
import { Colors } from '../constants';

const StyledLabel = styled(Text)(props => ({
  fontSize: 12,
  fontWeight: 'bold',
  color: props.focused ? props.theme.brownColor : 'rgb(150, 150, 150)',
  ...props.style,
}));

const StyledHelper = styled(Text)(props => ({
  fontSize: 10,
  color: props.error ? props.theme.dangerColor : props.theme.brownColor,
  paddingTop: props.theme.smGutter,
}));

const StyledTextInput = styled(TextInput)(({ isValid }) => ({
  flex: 1,
  height: 38,
  color: isValid ? 'black' : 'rgb(200, 53, 43)',
}));

const StyledMaskedTextInput = StyledTextInput.withComponent(TextInputMask);

const StyledAccessory = styled(View)(() => ({
  height: 38,
}));

const Container = styled(View)(({ detailColor }) => ({
  flexDirection: 'row',
  alignSelf: 'stretch',
  alignItems: 'center',
  borderBottomWidth: 1,
  borderBottomColor: detailColor,
}));

export default class Field extends Component {
  static propTypes = {
    onRef: PropTypes.func,
    label: PropTypes.string,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    labelStyles: PropTypes.object,
    rightAccessory: PropTypes.node,
    containerStyles: PropTypes.object,
    isValid: PropTypes.bool,
  };

  static defaultProps = {
    onRef: () => null,
    label: null,
    onFocus: () => null,
    onBlur: () => null,
    rightAccessory: null,
    containerStyles: {},
    labelStyles: {},
    isValid: true,
  };

  state = {
    focused: false,
  };

  componentDidMount() {
    this.props.onRef(this);
  }

  focus = () => {
    try {
      TextInputState.focusTextInput(findNodeHandle(this.ref));
    } catch (error) {
      console.log(error);
    }
  };

  blur = () => {
    try {
      TextInputState.blurTextInput(findNodeHandle(this.ref));
    } catch (error) {
      console.log(error);
    }
  };

  onFocus = () => {
    this.setState({ focused: true });
    this.props.onFocus();
  };

  onBlur = () => {
    this.setState({ focused: false });
    this.props.onBlur();
  };

  render() {
    const {
      containerStyles,
      label,
      error,
      editable = true,
      legend,
      placeholder,
      rightAccessory,
      onFocus,
      onBlur,
      labelStyles,
      isValid,
      ...props
    } = this.props;
    const { focused } = this.state;
    const TextInputComponent = props.type ? StyledMaskedTextInput : StyledTextInput;

    let detailColor = Colors.lineColor;
    if (!isValid) {
      detailColor = Colors.secondaryColor;
    } else if (focused) {
      detailColor = Colors.primaryColor;
    }
    return (
      <TouchableOpacity
        style={[{ alignSelf: 'stretch', opacity: !editable ? 0.6 : 1 }, containerStyles]}
        onPress={() => this.focus()}
        activeOpacity={1}
      >
        <StyledLabel focused={focused} style={labelStyles}>
          {label || ''}
        </StyledLabel>
        <Container detailColor={detailColor}>
          <TextInputComponent
            ref={(ref) => {
              this.ref = ref;
            }}
            isValid={isValid}
            placeholder={placeholder}
            placeholderTextColor="rgba(38, 12, 13, 0.48)"
            underlineColorAndroid="transparent"
            onFocus={() => this.onFocus()}
            onBlur={() => this.onBlur()}
            editable={editable}
            {...props}
          />
          <StyledAccessory>{rightAccessory}</StyledAccessory>
        </Container>
        {(error || legend) && <StyledHelper error={error}>{error || legend}</StyledHelper>}
      </TouchableOpacity>
    );
  }
}
