/* eslint-disable react/no-unused-prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, TouchableOpacityProps, Text, ActivityIndicator } from 'react-native';
import styled from 'styled-components';

const StyledTouchable = styled(TouchableOpacity)(props => ({
  flexDirection: 'row',
  height: 48,
  borderRadius: props.rounded ? 24 : 0,
  alignSelf: 'stretch',
  alignItems: 'center',
  justifyContent: 'center',
  borderWidth: 1,
  borderColor: props.theme[`${props.kind}Color`],
  backgroundColor: props.clear
    ? 'transparent'
    : props.theme[`${props.kind}${props.disabled ? 'Weak' : ''}Color`],
  ...props.style,
}));

const StyledText = styled(Text)(props => ({
  fontSize: 16,
  fontWeight: 'bold',
  color: props.textColor,
}));

class Button extends Component {
  static propTypes = {
    kind: PropTypes.oneOf(['paper', 'primary', 'secondary']),
    textColor: PropTypes.string,
    rounded: PropTypes.bool,
    ...TouchableOpacityProps,
  };

  static defaultProps = {
    kind: 'secondary',
    textColor: 'white',
    rounded: true,
  };

  render() {
    const { text, textColor, loading, ...props } = this.props;
    return (
      <StyledTouchable activeOpacity={0.8} {...props}>
        <StyledText textColor={textColor}>{text}</StyledText>
        {loading && <ActivityIndicator style={{ marginLeft: 16 }} color={textColor} />}
      </StyledTouchable>
    );
  }
}

export default Button;
