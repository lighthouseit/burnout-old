import React from 'react';
import PropTypes from 'prop-types';
import { Platform, KeyboardAvoidingView, ScrollView, View } from 'react-native';
import styled from 'styled-components';

const StyledKeyboardAvoidingView = styled(KeyboardAvoidingView)(({ theme, clear }) => ({
  flex: 1,
  backgroundColor: clear ? 'transparent' : theme.backgroundColor,
}));

const StyledScreen = styled(View)(({ theme, clear }) => ({
  flex: 1,
  backgroundColor: clear ? 'transparent' : theme.backgroundColor,
}));

const StyledScrollView = styled(ScrollView)({
  flexGrow: 1,
  backgroundColor: 'transparent',
});

const StyledView = styled(View)({
  flex: 1,
  backgroundColor: 'transparent',
});

const AVOID_VIEW_BEHAVIOR = Platform.OS === 'android' ? 'height' : 'padding';

class Screen extends React.PureComponent {
  static propTypes = {
    headerComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
    scrollEnabled: PropTypes.bool,
    clear: PropTypes.bool,
    avoidKeyboard: PropTypes.bool,
  };

  static defaultProps = {
    headerComponent: null,
    scrollEnabled: false,
    clear: false,
    avoidKeyboard: true,
  };

  static navigationOptions = {
    tabBarVisible: false,
  };

  render() {
    const {
      headerComponent,
      footerComponent,
      scrollEnabled,
      avoidKeyboard,
      children,
      clear,
      ...props
    } = this.props;

    if (!scrollEnabled) {
      return (
        <StyledKeyboardAvoidingView
          behavior={AVOID_VIEW_BEHAVIOR}
          enabled={avoidKeyboard}
          clear={clear}
        >
          <StyledScreen clear={clear}>
            {headerComponent}
            <StyledView {...props}>{children}</StyledView>
            {footerComponent}
          </StyledScreen>
        </StyledKeyboardAvoidingView>
      );
    }

    return (
      <StyledKeyboardAvoidingView
        behavior={AVOID_VIEW_BEHAVIOR}
        enabled={avoidKeyboard}
        clear={clear}
      >
        <StyledScreen clear={clear}>
          {headerComponent}
          <StyledScrollView {...props} keyboardShouldPersistTaps="handled">
            {children}
          </StyledScrollView>
          {footerComponent}
        </StyledScreen>
      </StyledKeyboardAvoidingView>
    );
  }
}

export default Screen;
