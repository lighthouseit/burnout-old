import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import { Colors } from '@constants';
import { Icon } from '@components';

import Field from './Field';

const StyledTouchable = styled(TouchableOpacity)(() => ({
  flex: 1,
  justifyContent: 'center',
  opacity: 0.5,
}));

export default class SecureField extends Component {
  state = {
    secure: true,
  };

  toggleSecure = () => {
    this.setState(prevState => ({ secure: !prevState.secure }));
  };

  render() {
    const { secure } = this.state;
    return (
      <Field
        rightAccessory={(
          <StyledTouchable onPress={() => this.toggleSecure()}>
            <Icon name={secure ? 'eye' : 'eye-off'} size={24} color={Colors.brownColor} />
          </StyledTouchable>
)}
        secureTextEntry={this.state.secure}
        autoCorrect={false}
        {...this.props}
      />
    );
  }
}
