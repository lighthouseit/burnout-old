# Burnout

> Your apps, now HOT!

[English](https://gitlab.com/lighthouseit/burnout/blob/master/README.en.md) | [Português](https://gitlab.com/lighthouseit/burnout/blob/master/README.md)

![Lighthouse Utils](https://i.imgur.com/VpKKfIM.png)

The official React Native boilerplate by Lighthouse

This is the boilerplate used and recommended by the Lighthouse team.

We also recommend that you use our CLI [Lighthouse Tools](https://gitlab.com/lighthouseit/lighthouse-tools) to start your development quickly.

# Guides

- [Setup Guide](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP.en.md) - _Required_
- [Setup CI/CD](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-CI.en.md)
- [Install iOS CocoaPods](https://gitlab.com/lighthouseit/burnout/blob/master/docs/INSTALL-PODS.en.md)
- [Setup Facebook SDK](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-FACEBOOK-SDK.en.md) - _Required_
- [Setup Google Signin](https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-GOOGLE-SIGNIN.en.md)

# Quick start

Using the `Lighthouse Tools`:

```
$ yarn global add lighthouse-tools
$ lighthouse-tools init awesomeapp

// Installing dependencies
$ cd awesomeapp
$ yarn install
$ cd ios && pod install

// Running on Android
$ yarn run-android

// Running on iOS
$ yarn run-ios

🚀  1, 2, 3... Ready!
```

Cloning repo way:

```
$ git clone https://gitlab.com/lighthouseit/burnout awesomeapp

// Installing dependencies
$ cd awesomeapp
$ yarn install
$ cd ios && pod install

// Running on Android
$ yarn run-android

// Running on iOS
$ yarn run-ios

🚀  1, 2, 3... Ready!
```

## Environment variables

We encourage you to use environment variables in your project, because of this we use the `react-native-config` library to manage our variables within the JS files.

To begin, duplicate and rename the `.env.example` file to `.env` and read the instructions inside the file.

## Already linked Native Modules

- lottie-react-native
- react-native-config
- react-native-device-info
- react-native-fast-image
- react-native-fbsdk
- react-native-google-signin
- react-native-google-analytics-bridge
- react-native-image-gallery
- react-native-languages
- react-native-linear-gradient
- react-native-maps
- react-native-masked-text
- react-native-permissions
- react-native-snap-carousel
- react-native-svg
- react-native-vector-icons

[Node.js 7.6+](https://nodejs.org) is required.

## Sponsors

[Burnout](https://gitlab.com/lighthouseit/burnout) is sponsored by [Lighthouse](https://lighthouseit.com.br/), a team of designers and developers located in Porto Alegre / RS - Brazil. Our skills go from UI/UX, React, React Native, .NET Core and more!
