import React from 'react';
import { Platform, StatusBar, YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { PersistGate } from 'redux-persist/lib/integration/react';
// import { GoogleSignin } from 'react-native-google-signin';
import RootContainer from './src/containers/RootContainer';
import createStore from './src/redux';
import Theme from './src/constants/Theme';

const { store, persistor } = createStore();

export default class App extends React.Component {
  componentDidMount() {
    YellowBox.ignoreWarnings([
      'Using `apparitionDelay` on Android is not recommended since it can lead to rendering issues',
    ]);
    if (Platform.OS === 'android') {
      StatusBar.setTranslucent(false);
      StatusBar.setBackgroundColor(Theme.statusBarColor);
    }

    /** ***********************************************************
     * Google Signin configuration
     * Follow this guide to get started:
     * https://gitlab.com/lighthouseit/burnout/blob/master/docs/SETUP-GOOGLE-SIGNIN.md
     ************************************************************
     */
    // GoogleSignin.configure({
    //   webClientId: 'xxxx.apps.googleusercontent.com',
    //   forceConsentPrompt: true,
    // });
  }

  render() {
    return (
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <RootContainer />
          </PersistGate>
        </Provider>
      </ThemeProvider>
    );
  }
}
