jest.mock('react-native-languages', () => ({
  RNLanguages: {
    language: 'pt',
    languages: ['pt'],
  },
}));

jest.mock('react-native-device-info', () => ({
  isEmulator: jest.fn(),
}));

jest.mock('react-native-config', () => ({
  RN_CDN_URL: 'http://myclient.lighthouseit.com.br/cdn',
  RN_API_URL: 'http://myclient.lighthouseit.com.br',
}));
