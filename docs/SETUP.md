# Guia de configuração

> Guia de configurações necessárias para rodar o projeto.

## Configurando Firebase SDK (Android)

**Crie seu projeto no Firebase e siga os passos abaixo**.

Adicione seu aplicativo para Android dentro das configurações do projeto.

![Step 1](https://i.imgur.com/bMvCxkQ.png)

Desça a página e clique no botão "Add app".

![Step 2](https://i.imgur.com/1QH7PEq.png)

Preencha os dados dos campos.

![Step 3](https://i.imgur.com/nMluPa4.png)

Para o React Native é obrigatório adicionar a hash SHA-1 no campo _Debug signin certificate SHA-1_.

Você pode usar o hash SHA1 do seu keystore de debug, para obtê-lo execute o comando abaixo.
_Caso seja solicitado uma senha, digite "android"_

`keytool -exportcert -keystore ~/.android/debug.keystore -list -v`

![Step 4](https://i.imgur.com/Xqi57uo.png)

Para obter o hash de produção SHA1, você precisa gerar seu keystore, para gerar seu keystore, [siga este guia](https://facebook.github.io/react-native/docs/signed-apk-android.html).

Baixe e adicione o arquivo de configuração gerado `google-services.json` em `<YOUR_PROJECT_ROOT>/android/app`

![Step 5](https://i.imgur.com/9lSlQw3.png)

## Configurando Firebase SDK (iOS)

_Em breve_
